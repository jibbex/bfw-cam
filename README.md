# BFW CAM

IP Kamera Video Streaming und Steuerung

![](https://git.michm.de/manne/bfw-cam/-/raw/main/assets/v.mp4)

## Einstellungen

Im Hauptverzeichnis liegt die Datei `cam.ini`. Dort kann die IP Adresse der Kamera eingestellt werden.

```conf
[network]
host = 192.168.19.218
protocol = http
```

## **Ungelöstes Problem**

Es gibt ein Input Lag von ca. 1s beim Steuern der Kamera. Sie lässt sich mit ein wenig Geschick dennoch präzise genug steuern. Dieser besteht jedoch nicht bei Nutzung des Web Interfaces. Was darauf schließen lässt, dass es ein Problem mit der der aktuellen Implementation ist. Folgende Dinge habe ich bereits versucht:

* Requests vom Interface nochmal genaustens analysiert. Ist exakt gleich
* ffPlay zum streamen verwendet (hat sogar eine spürbar höhere Latenz)

> [Test Auswertung: HikVision SDK](https://git.michm.de/manne/bfw-cam#testergebnisse).

### Weitere Lösungsstrategien

~~Einen Client in C und mit dem Vendor SDK realisieren und diesen dann auch noch mal Vergleichen. Auch hier genügt es, wenn ich auschließlich das Streamen implementiere.~~

Ich habe in der Vendor SDK Dokumentation je ein Beispiel Projekt in C# (WinForms) und C (Win32 API) gefunden. Die sollten zum Testen mehr als ausreichend sein.

Anstatt der VLC Lib, könnte ich eine DirectShow Implementation als Plugin für Flutter realisieren. Ich denke aber nicht das es besser laufen wird als der C# - Prototyp. Außerdem ist das sehr aufwendig im Vergleich. Wenn das SDK gut funktioniert sollte ich das lieber einbetten. 

> ~~Das [flutter_native_view](https://github.com/alexmercerind/flutter_native_view) Plugin könnte mir jede menge Arbeit bei einem Implementationsversuch abnehmen.~~ Wobei sich schon die Frage aufdrängt ob es nicht viel einfacher wäre einen weniger schönen C# WPF Client aus dem Prototyp zu entwickeln als die brücke von Nativen Code zu Dart zu schlagen.

**Letzte Möglichkeit: Damit leben.**

### Weitere kleine Ärgernisse

Die Discover-Funktion läuft im BFW Netzwerk nur zum Teil und endet nie. Das Debugging gestaltet sich schwierig, weil Android Studio die Anwendung nicht mit dem Debugger verbinden mag. Ich habe keinen Schimmer warum es nicht funktioniert. Das hat aber gerade keine Priorität.

~~Die fehlende DLL habe ich noch nicht ausmachen können. Es läuft ohne Probleme sowohl bei Armella als auch Benny.~~ Wie erwartet fehlte schlichst die **VC Redist x64**. _Ich habe CMake so konfiguriert, das beim Erstellen alle nötigen shared Libs mit in das Build Verzeichniss kopiert werden._

## Testergebnisse
Bei der C# WinForms Anwendung fehlte das korrekte .Net Framework und der Proxy verursachte wieder Probleme, die meine vollständige Aufmerksamkeit benötigt hätten. Die C Anwendung konnte sich im ISAPI Modus verbinden, dekodierte den Stream und spielte ihn auch ab.
Das Fenster war in einer fixen Größe und die Auflösung war stark reduziert. Die Reaktion zur Eingabe erfolgte umittelbar. Eine Kausalität
zur geringen Qualität lässt sich allerdings nicht auschliessen. Die PTZ Steuerung verweigerte mit einer nichtssagenden Fehlermeldung den
Dienst.

> Sollte tatsächlich ein Zusammenhang zwischen Qualität und Reaktionszeit in beobachteter Höhe vorhanden sein, wird ein Wechsel zur
> niedrigeren Auflösung beim Steuervorgang und das Erhöhen danach das Problem minimieren. Gut wäre auch die Möglichkeit die Auflösung
> konfigurierbar zumachen. Meiner Einschätzung nach sollte aber die Latenz so gering sein, das sie nicht weiter ins Gewicht fällt.
>
> Derzeit bewegt sich alles auf eine Implementation ohne VLC und dafür mit dem HikVision SDK zu. Morgen folgen weitere Tests. Diesmal
> mit einer kleinen C Anwendung. Langsam verstehe ich sehr genau warum sie WebSocket zum Streaming verwenden...

### Update zu meinen Experimenten
Ich habe es geschaft die HCNetSDK.dll in Dart mittels `ffi` zu laden, zwei Funktionen daraus zu adressieren und aufzurufen. Die erste 
war simpel, da ihre Signatur den Rückgabewert `void` und keine Parameter enthielt. Die zweite gab ein `DWORD` zurück.

```dart
typedef NativeInitSdkFunc = ffi.Void Function();
typedef NativeSdkVer = ffi.Uint32 Function();
typedef SdkVer = int Function();
typedef InitSdk = void Function();

ffi.DynamicLibrary _hkLib = ffi.DynamicLibrary.open(path.join(Directory.current.path, 'HCNetSDK.dll'));

final InitSdk initSdk = _hkLib.lookup<ffi.NativeFunction<NativeInitSdkFunc>>('NET_DVR_Init').asFunction();
final SdkVer sdkVer = _hkLib.lookup<ffi.NativeFunction<NativeSdkVer>>('NET_DVR_GetSDKBuildVersion').asFunction();

// Gibt die SDK Version zurück
String ver() {
    initSdk();
    final v = sdkVer();
    final version = 'v${((0xff000000 & v) >> 24)}.${((0x00ff0000 & v) >> 16}.${(0x0000ff00 & v) >> 8}.${0x0000000ff & v}';
    return version;
}
```

> Ich sollte mich eigentlich nicht mehr Zeit in das Thema versenken. Zumal ich nicht weiß ob eine neuere oder ältere Version der 
> vlclib das Problem nicht schon behebt. Einige Forenbeiträge lassen zumindest darauf schließen. Nur bin ich zwingend durch eine 
> Funktion an eine 3x Version gebunden. Ich überdenke das ganze nach den nächsten Test an der Kamera noch einmal.

---

> Ganz offenbar liegt es nicht an der Art und Weise wie `libvlc` den Stream dekodiert, sondern am verwendeten Protokoll. Das HikVision
> SDK hat eine ähnlich hohe Latenz, wenn RTSP zur Übertragung verwendet wird. Bei TCP und auch UDP besteht noch ein minimaler Delay.
> ### Ich versuche doch tatsächlich ein Subset vom HCNetSDK und einen GPU beschleunigten Dekoder zu implementieren ⇢ [hik_vision_lib](https://git.michm.de/manne/hik_vision_lib)

---

## VLC playlist.xspf

```xml
<?xml version="1.0" encoding="UTF-8"?>
<playlist xmlns="http://xspf.org/ns/0/" xmlns:vlc="http://www.videolan.org/vlc/playlist/ns/0/" version="1">
    <title>Wiedergabeliste</title>
    <trackList>
        <track>
            <location>rtsp://xxxxx:xxxxx@192.168.13.168</location>
            <extension application="http://www.videolan.org/vlc/playlist/0">
                <vlc:id>0</vlc:id>
                <vlc:option>network-caching=1000</vlc:option>
            </extension>
        </track>
    </trackList>
    <extension application="http://www.videolan.org/vlc/playlist/0">
        <vlc:item tid="0"/>
    </extension>
</playlist>
```

## HTTP Requests

### Login

```
Anforderungs-URL: http://192.168.13.168/doc/page/login.asp
Anforderungsmethode: GET
Statuscode: 200 OK
Remoteadresse: 192.168.13.168:80
Referrer-Richtlinie: strict-origin-when-cross-origin
---
Request:
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: de
Connection: keep-alive
Cookie: language=de; WebSession_9085523c7a=006d7cdf677eb2a422fce7eb890f5be680f55ced936e53ac9871498427748ff7; _wnd_size_mode=4
Host: 192.168.13.168
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.53
---
Response:
Connection: keep-alive
Content-Length: 4966
Content-Type: text/html
Date: Thu, 06 Oct 2022 10:22:39 GMT
ETag: "4c0-1366-60e3f952"
Keep-Alive: timeout=8, max=98
Last-Modified: Tue, 06 Jul 2021 06:33:54 GMT
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block

# Nach Login redirect zu =>

Anforderungs-URL: http://192.168.13.168/doc/page/preview.asp
Anforderungsmethode: GET
Statuscode: 200 OK
Remoteadresse: 192.168.13.168:80
Referrer-Richtlinie: strict-origin-when-cross-origin
---
Request:
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: de,de-DE;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6
Connection: keep-alive
Cookie: _wnd_size_mode=5; language=de; WebSession_9085523c7a=066092a5cd42b85fcca1b3bf83b0834cb553420bdd0009c803e49c173497c574
Host: 192.168.13.168
Referer: http://192.168.13.168/doc/page/login.asp?_1665043547483
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.53
---
Response:
Connection: keep-alive
Content-Length: 4517
Content-Type: text/html
Date: Thu, 06 Oct 2022 10:06:48 GMT
ETag: "4f8-11a5-60e3f952"
Keep-Alive: timeout=8, max=85
Last-Modified: Tue, 06 Jul 2021 06:33:54 GMT
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
```

### Steuerung

- Steuerung wird mittels HTTP PUT Request an die URI `/ISAPI/PTZCtrl/channels/<n>/continuous` gehandled
- Body des Requests ist ein XML Dokument
- Element `PTZData `ist das Elternelement
- Weitere Elemente: `zoom`, `pan`, `titlt`

*Siehe Beispiel...*

```
Anforderungs-URL: http://192.168.13.168/ISAPI/PTZCtrl/channels/1/continuous
Anforderungsmethode: PUT
Statuscode: 200 OK
Remoteadresse: 192.168.13.168:80
Referrer-Richtlinie: strict-origin-when-cross-origin
---
Request:
Accept: */*
Accept-Encoding: gzip, deflate
Accept-Language: de,de-DE;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6
Cache-Control: max-age=0
Connection: keep-alive
Content-Length: 72
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
Cookie: language=de; WebSession_9085523c7a=066092a5cd42b85fcca1b3bf83b0834cb553420bdd0009c803e49c173497c574; _wnd_size_mode=4; sdMarkTab_1_0=0%3AsettingBasic; sdMarkMenu=2_0%3Anetwork; szLastPageName=network%3Cbasic; sdMarkTab_2_0=0%3AbasicTcpIp
Host: 192.168.13.168
If-Modified-Since: 0
Origin: http://192.168.13.168
Referer: http://192.168.13.168/doc/page/preview.asp
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36 Edg/105.0.1343.53
X-Requested-With: XMLHttpRequest

Body:
<?xml version="1.0" encoding="UTF-8"?><PTZData><zoom>60</zoom></PTZData>
---
Response:
Cache-Control: no-cache
Connection: keep-alive
Content-Length: 291
Content-Type: application/xml
Date: Thu, 06 Oct 2022 10:27:28 GMT
Keep-Alive: timeout=8, max=90
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block

Body:
<ResponseStatus xmlns="http://www.hikvision.com/ver20/XMLSchema"version="2.0">
    <requestURL>/ISAPI/PTZCtrl/channels/1/continuous</requestURL>
    <statusCode>1</statusCode>
    <statusString>OK</statusString>
    <subStatusCode>ok</subStatusCode>
</ResponseStatus>
```

**Weiteres Beispiel f. Request Body:**

```xml
<?xml version: "1.0" encoding="UTF-8"?><PTZData><pan>60</pan><tilt>0</tilt></PTZData>
```
