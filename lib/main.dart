import 'dart:io';

import 'package:bfw_cam/src/pages/video_page.dart';
import 'package:bfw_cam/src/theme/theme.dart';
import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:dart_vlc/dart_vlc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Application entry-point. If cam.ini doesn't exists, creates it with
/// an default host value as example. Otherwise it passes the reference
/// to _parseConfig function and sets the result to conf object. DartVLC
/// will be initialized and inflaters the root widget with a call of
/// runApp. An Exception is thrown, if the conf Map object is empty
/// after calling _parseConfig.
void main() async {
  Map<String, dynamic> conf = {};
  final cfgFile = File('cam.ini');

  if (await cfgFile.exists()) {
    conf = await _parseConfig(cfgFile);
  } else {
    const iniContent = """
    [network]
    host = 192.168.19.218
    protocol = http
    debug = false
    #file = path/to/video
    """;

    conf = {
      'host': '192.168.19.218',
      'protocol': 'http',
      'debug': false,
    };

    cfgFile.writeAsString(iniContent);
  }

  await DartVLC.initialize();

  runApp(CamApp(conf));

  doWhenWindowReady(() {
    appWindow.minSize = const Size(360, 280);
    appWindow.size = const Size(1280, 760);
    appWindow.alignment = Alignment.center;
    appWindow.title = "BFW Cam";
    appWindow.show();
  });
}

/// Reads the passed file objects contents as String in lines. On Each line
/// that contains a equal sign '=', will be mapped => [key] = [value] and
/// added to the config map object. An future of Map<String, dynamic> will
/// be returned.
Future<Map<String, dynamic>> _parseConfig(File config) async {
  Map<String, dynamic> mappedConf = {};

  try {
    final lines = await config.readAsLines();

    for (final line in lines) {
      if (line.contains('=') && !line.startsWith('#')) {
        final keyVal = line.split('=');

        if (keyVal.length == 2) {
          mappedConf.putIfAbsent(keyVal[0].trim(), () => keyVal[1].trim());
        } else if (kDebugMode) {
          print('Malformed config: $line');
        }
      }
    }
  } catch (e) {
    if (kDebugMode) {
      print(e);
    }
  }

  return mappedConf;
}

class CamApp extends StatelessWidget {
  /// Parsed config
  final Map<String, dynamic> config;
  const CamApp(this.config, {super.key});

  @override
  Widget build(BuildContext context) {
    final themeData = theme(context, true);
    return MaterialApp(
      title: 'BFW Cam',
      theme: themeData,
      home: VideoPage(
        title: 'BFW Cam',
        config: config,
      ),
      routes: {
        '/stream': videoPageBuilder,
      },
    );
  }

  Widget videoPageBuilder(BuildContext context) => VideoPage(
    title: 'BFW Cam',
    config: config,
  );
}
