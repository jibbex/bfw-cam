import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:http_auth/http_auth.dart';
import 'hik_client_exception.dart';

/// PTZ control actions
enum PtzCtrl { zoomIn, zoomOut, pan }

/// Directions of movement
enum Direction { left, top, right, bottom }

/// API endpoints
enum Endpoint {
  auth('${HikClient._kPathSecurity}userCheck'),
  ptz('${HikClient._kPathPTZCtrl}channels/'),
  deviceInfo('${HikClient._kPathSystem}deviceInfo'),
  image('/ISAPI/image/channels/');

  final String path;

  const Endpoint(this.path);
}

/// Client for IP Camera web interface. Handles authentication, sends control
/// requests and reacts to responses.
///
/// For user authentication ISAPI protocol uses HTTP basic authentication and
/// digest authentication. Login can be requested via an HTTP GET request on
/// path: `/ISAPI/Security/userCheck`.
class HikClient {
  /// ISAPI Predefined URIs
  ///
  /// System related resources
  static const _kPathSystem = '/ISAPI/System/';

  /// Security related resources
  static const _kPathSecurity = '/ISAPI/Security/';

  // Video streaming and management related resources
  // static const _kPathStreaming = '/ISAPI/Streaming/';

  // Event/alarm related resources
  // static const _kPathEvent = '/ISAPI/Event/';

  /// PTZ control related resources
  static const _kPathPTZCtrl = '/ISAPI/PTZCtrl/';

  static const Set<Direction> _panning = {Direction.left, Direction.right};
  static const Set<Direction> _tilting = {Direction.top, Direction.bottom};

  int channel;

  /// Base URL of the web server
  final Uri url;

  /// Credentials of the account that has access to
  /// the web interface.
  final String username;
  final String password;

  /// HTTP client that implements digest authentication.
  final DigestAuthClient _client;

  /// Constructor of HikClient class
  HikClient(String host, {
    int port = 80,
    String protocol = 'http',
    this.channel = 1,
    required this.username,
    required this.password,
  })
      : url = Uri(
    host: host,
    port: port,
    scheme: protocol,
  ),
        _client = DigestAuthClient(username, password);

  /// Requests user authentication. Returns a Future which will resolve
  /// to an Response object on success.
  Future<Response> login() async => _client.get(_composeUri(Endpoint.auth));

  /// Sends an HTTP PUT request to update the zoom state of the camera
  Future<void> zoom(PtzCtrl ctrl) async {
    if (ctrl != PtzCtrl.zoomIn && ctrl != PtzCtrl.zoomOut) {
      throw InvalidZoomException(ctrl, 'ctrl');
    }

    if (kDebugMode) print('zoom start');

    await _zoom(PtzCtrl.zoomOut == ctrl ? -60 : 60);

    final res =
    await _client.get(_composeUri(Endpoint.ptz, '$channel/continuous'));

    if (kDebugMode) {
      print(res);
    }
  }

  Future<void> zoomStop() async {
    if (kDebugMode) print('zoom stop');
    _zoom(0);
  }

  Future<Response> _zoom(int zoom) =>
      _client.put(
        _composeUri(Endpoint.ptz, '$channel/continuous'),
        body: _xmlEncode({
          'PTZData': <String, dynamic>{'zoom': zoom},
        }),
      );

  /// Pans and tilts the camera in direction that was passed. Throws an
  /// Exception, when the passed Set contains contradictory directions.
  /// e.g. Direction.left && Direction.right
  Future<void> ptzMove(Set<Direction> dir) async {
    bool validPanning =
        dir.contains(Direction.left) && !dir.contains(Direction.right) ||
            !dir.contains(Direction.left) && dir.contains(Direction.right) ||
            !dir.contains(Direction.left) && !dir.contains(Direction.right);

    bool validTilting =
        dir.contains(Direction.top) && !dir.contains(Direction.bottom) ||
            !dir.contains(Direction.top) && dir.contains(Direction.bottom) ||
            !dir.contains(Direction.top) && !dir.contains(Direction.bottom);

    if (!validPanning || !validTilting) {
      throw InvalidDirectionException(dir, 'dir');
    }

    if (kDebugMode) {
      print('start: $dir');
    }

    int pan = 0;
    int tilt = 0;

    if (dir.any(_panning.contains)) {
      pan = dir.contains(Direction.left) ? -60 : 60;
    }

    if (dir.any(_tilting.contains)) {
      tilt = dir.contains(Direction.bottom) ? -60 : 60;
    }

    await _client.put(
      _composeUri(Endpoint.ptz, '$channel/continuous'),
      body: _xmlEncode({
        'PTZData': <String, dynamic>{
          'pan': pan,
          'tilt': tilt,
        }
      }),
    );
  }

  /// Stops panning and tilting of the camera
  Future<void> ptzStop() async {
    if (kDebugMode) {
      print('stop ptz ctrl');
    }

    _client.put(
      _composeUri(Endpoint.ptz, '$channel/continuous'),
      body: _xmlEncode({
        'PTZData': <String, dynamic>{
          'pan': 0,
          'tilt': 0,
        }
      }),
    );
  }

  /// Returns the http request uri.
  Uri _composeUri(Endpoint endpoint, [String? path]) =>
      Uri.parse('$url${endpoint.path}${path ?? ''}');

  /// Encodes the passed Map object to a XML encoded string.
  String _xmlEncode(Map<String, dynamic> map) =>
      '<?xml version="1.0" encoding="UTF-8"?>${_createXmlNodes(map)}';

  /// Traverses the passed Map object and creates XML tags from the keys and
  /// XML values from values of the map, concatenates each element to a string
  /// and returns the string.
  String _createXmlNodes(Map<String, dynamic> map) {
    var xmlNode = '';

    for (final entry in map.entries) {
      if (entry.value is Map) {
        xmlNode +=
        '<${entry.key}>${_createXmlNodes(entry.value)}</${entry.key}>';
      } else {
        xmlNode += '<${entry.key}>${entry.value}</${entry.key}>';
      }
    }

    return xmlNode;
  }

  /// Calculates the IP range of all possible clients in the network. Creates
  /// for each address an HTTPClient object in an Isolate and sends a HTTP
  /// HEAD request to it on path /ISAPI/security/userCheck. Listens on IPC
  /// channel to the Isolates funnels the results in a Stream. The first data
  /// added to the Stream is the integer number of addresses on which a
  /// connection will be opened. When one HTTPClient responses with 403 status
  /// code, the HttpHeader object is added to the sink of the stream and all
  /// remaining Isolates will be killed. The Stream object will be returned.
  ///
  /// TODO: fix address iteration on networks with a mask < 24
  static Future<Stream> discover(String ipAddress, String mask) async {
    const int kPort = 80;
    final streamCtrl = StreamController();
    final List<Isolate> isolates = [];
    final bytes = ipAddress.split('.').map((el) => int.parse(el)).toList();
    final masked = int.parse(mask);
    final exp = masked % 8;
    final netmask = bytes;
    int netId = -1;
    int netCount = pow(2, exp).toInt();
    int addressRange = 256 ~/ netCount;
    int byteIndex = 0;

    if (masked >= 24) {
      byteIndex = 3;
    } else if (masked >= 16) {
      byteIndex = 2;
    } else if (masked >= 8) {
      byteIndex = 1;
    }

    // For debugging purposes needed
    switch (exp) {
      case 1:
        netmask[byteIndex] = 128;
        break;
      case 2:
        netmask[byteIndex] = 192;
        break;
      case 3:
        netmask[byteIndex] = 224;
        break;
      case 4:
        netmask[byteIndex] = 240;
        break;
      case 5:
        netmask[byteIndex] = 248;
        break;
      case 6:
        netmask[byteIndex] = 252;
        break;
      case 7:
        netmask[byteIndex] = 254;
        break;
      default:
        netmask[byteIndex] = 255;
    }

    for (int i = byteIndex; i < netmask.length; i++) {
      netmask[i] = 0;
    }

    for (int i = 0; i < 256; i += addressRange) {
      if (bytes[byteIndex] >= i && bytes[byteIndex] < i + addressRange) {
        netId = i;
        break;
      }
    }

    if (netId == -1) {
      throw NetNotFoundException(
          netmask: netmask.map((e) => e.toString()).join(),
          address: ipAddress,
          mask: mask,
          exp: exp,
          count: netCount,
          range: addressRange,
          message: "Network ID wasn't found."
      );
    }

    streamCtrl.sink.add(addressRange - 2);

    for (int i = netId + 1; i <= addressRange - 1; i++) {
      bytes[byteIndex] = i;

      final receivePort = ReceivePort();
      final args = <String, dynamic>{
        'host': bytes.join('.'),
        'port': kPort,
        'timeout': 2,
        'sendPort': receivePort.sendPort,
      };

      isolates.add(
        await Isolate.spawn(
          _headReq,
          args,
          debugName: 'request to: ${args['host']}',
        ),
      );

      receivePort.listen((data) {
        if (data is HttpClientResponse) {
          if (data.statusCode == 403) {
            streamCtrl.sink.add(data);
            for (var isolate in isolates) {
              isolate.kill();
            }
          } else {
            streamCtrl.sink.add(data.reasonPhrase);
          }
        } else {
          streamCtrl.sink.add(data);
        }
      }, onError: (error) => streamCtrl.sink.add(error));
    }

    return streamCtrl.stream;
  }

  /// Is supposed to be executed in an Isolate.
  ///
  /// Creates an HTTPClient object and sends an HEAD request to the host, which
  /// is defined in the args Map. Asserts all required properties of the passed
  /// Map before execution. Send the result of the connection attempt via an
  /// SendPort object back to the parent process.
  static Future<void> _headReq(Map<String, dynamic> args) async {
    assert(args['host'] is String);
    assert(args['timeout'] is int);
    assert(args['port'] is int);
    assert(args['sendPort'] is SendPort);
    assert(args['port'] >= 0 && args['port'] < 65535);
    assert(args['timeout'] >= 0 && args['timeout'] <= 10);

    try {
      HttpClient client = HttpClient()
        ..connectionTimeout = Duration(seconds: args['timeout']);

      final req = await client.head(
        args['host'],
        args['port'],
        '${Endpoint.auth}',
      );

      (args['sendPort'] as SendPort).send(req.headers);
    } catch(e) {
      (args['sendPort'] as SendPort).send(e);
    }
  }
}
