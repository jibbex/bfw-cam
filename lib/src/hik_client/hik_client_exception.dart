import 'hik_client.dart' show Direction, PtzCtrl;

/// Base interface of all Exceptions related to hik_client's
/// ISAPI client interface.
abstract class HikClientException implements Exception {
  factory HikClientException([var name, var message]) =>
      _HikClientException(name, message);

  String get reportTitle;
}

/// Implementation of InvalidPtzCtrlException interface.
class _HikClientException implements HikClientException {
  final String? message;
  final String? name;

  _HikClientException([this.name, this.message]);

  @override
  String get reportTitle => 'InvalidPtzCtrlException';

  @override
  String toString() {
    String report = reportTitle;

    if (message != null && '' != message) {
      report = '$report: $message';
    }

    if (name != null) {
      report += ' (on argument "$name")\n';
    }

    return report;
  }
}

/// InvalidZoomException is thrown if the passed PtzCtrl argument is
/// neither zoomIn nor zoomOut.
class InvalidZoomException extends _HikClientException {
  final PtzCtrl control;

  InvalidZoomException(this.control, [super.name, super.message]);

  @override
  String get reportTitle => 'InvalidZoomException';

  @override
  String toString() =>
      '${super.toString()}\tPtzCtrl.zoomIn || PtzCtrl.zoomOut is required.\n';
}

/// InvalidDirectionException is thrown on contradictory Directions in a Set
/// and has a helpful message property with additional information, that will
/// be generated from the passed Directions Set. It is required on construction.
class InvalidDirectionException extends _HikClientException {
  final Set<Direction> directions;

  InvalidDirectionException(this.directions, [super.name, super.message]);

  @override
  String get reportTitle => 'InvalidDirectionException';

  @override
  String toString() {
    String report = super.toString();

    if (directions.contains(Direction.left) &&
        !directions.contains(Direction.right) ||
        !directions.contains(Direction.left) &&
            directions.contains(Direction.right)) {
      report += "\tDirection.left && Direction.right is not possible.\n";
    }

    if (directions.contains(Direction.top) &&
        !directions.contains(Direction.bottom) ||
        !directions.contains(Direction.top) &&
            directions.contains(Direction.bottom)) {
      report += "\tDirection.top && Direction.bottom is not possible.\n";
    }

    return report;
  }
}

/// NetNotFoundException is thrown, when an subnetwork of an CIDR based
/// IPv4 address was not found.
class NetNotFoundException extends _HikClientException {
  final String netmask;
  final String address;
  final String mask;
  final int exp;
  final int count;
  final int range;

  NetNotFoundException({
    String? message,
    required this.netmask,
    required this.address,
    required this.mask,
    required this.exp,
    required this.count,
    required this.range,
  }) : super(message);

  @override
  String get reportTitle => 'NetNotFoundException';

  @override
  String toString() {
    String report = super.toString();

    report = """
    IPv4 Address: $address / $mask    Netmask: $netmask                                       
    2^$exp = $count expected networks. Each with ${range - 2} possible clients.
    
    $message
    """;

    return report;
  }
}