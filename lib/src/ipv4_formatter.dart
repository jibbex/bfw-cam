import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class IPv4Formatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    final buffer = StringBuffer();
    final chars = newValue.text.characters.toList();
    final isRemoving = oldValue.text.length > newValue.text.length;
    final bytes = newValue.text.split('.');

    int selPos = newValue.selection.baseOffset;
    int byteOffset = 0;

    if (!isRemoving ) {
      if (bytes.length < 4 || bytes.length == 4 && bytes[3].length <= 3) {
        for (int i = 0; i < chars.length; i++) {
          if (RegExp(r'[\d /.]').hasMatch(chars[i])) {
            var isDot = _isDot(chars[i]);
            final j = i + 1;
            final notDoubleDotted =
                chars[i] == '.' && j < chars.length && chars[j] != '.';
            final dotIndex = chars.indexOf('.', i);

            if (byteOffset == 3 && !isDot) {
              buffer.write('.');
              isDot = true;
              selPos++;
            }

            if (dotIndex - 4 < i || !isDot && notDoubleDotted) {
              buffer.write(chars[i]);
              byteOffset++;
            }

            if (isDot) {
              byteOffset = 0;
            }
          }
        }
      } else {
        selPos = oldValue.selection.baseOffset;
        buffer.write(oldValue.text);
      }
    } else {
      buffer.write(newValue.text);
    }

    return TextEditingValue(
      text: buffer.toString(),
      selection: TextSelection.collapsed(offset: selPos),
    );
  }

  bool _isDot(String str) => str == '.';
}
