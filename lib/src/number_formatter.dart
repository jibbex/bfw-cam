import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NumberFormatter extends TextInputFormatter {
  final int maxDigits;
  final int? maxValue;

  NumberFormatter({
    required this.maxDigits,
    this.maxValue,
  });

  bool _isMoreThanMax(num value) =>
      maxValue == null ? false : value > maxValue!;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    final buffer = StringBuffer();
    final chars = newValue.text.characters.toList();
    final isRemoving = oldValue.text.length > newValue.text.length;

    if (!isRemoving) {
      for (int i = 0; i < chars.length; i++) {
        final value = num.tryParse('$buffer${chars[i]}') ?? 0;
        if (i == maxDigits) {
          break;
        }

        if (RegExp(r'[\d ]').hasMatch(chars[i]) && !_isMoreThanMax(value)) {
          buffer.write(chars[i]);
        }
      }
    } else {
      buffer.write(newValue.text);
    }

    return TextEditingValue(
      text: buffer.toString(),
      selection: TextSelection.collapsed(offset: buffer.length),
    );
  }
}
