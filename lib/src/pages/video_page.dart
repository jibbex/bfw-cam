import 'dart:io';
import 'dart:ui';

import 'package:bfw_cam/src/hik_client/hik_client.dart';
import 'package:bfw_cam/src/theme/colors.dart';
import 'package:bfw_cam/src/user.dart';
import 'package:bfw_cam/src/widgets/control_panel.dart';
import 'package:bfw_cam/src/widgets/window_buttons.dart';
import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:dart_vlc/dart_vlc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class VideoPage extends StatefulWidget {
  final Map<String, dynamic> config;
  final String title;

  const VideoPage({
    super.key,
    required this.title,
    required this.config,
  });

  @override
  State<VideoPage> createState() => _VideoPageState();
}

class _VideoPageState extends State<VideoPage> {
  late final String _host;
  late final Player _player;
  late final HikClient _client;
  late final bool _isDebugMode;

  // bool _isPlaying = true;

  @override
  initState() {
    if (widget.config['debug'] != null) {
      final debug = widget.config['debug'] as String;
      _isDebugMode = debug.toLowerCase().contains('true');
    } else {
      _isDebugMode = false;
    }

    _host = widget.config['host'];

    final stream = _isDebugMode && widget.config['file'] != null
        ? Media.file(
            File(widget.config['file']),
          )
        : Media.network("rtsp://$kUser:$kPass@$_host"); // Media.network('rtsp://localhost:8554/stream');

    _player = Player(id: 0);
    _client = HikClient(
      widget.config['host'],
      username: kUser,
      password: kPass,
    );

    _player.add(stream);
    _player.play();
    _player.currentController.onCancel = () {
      _player.play();
    };

    _client.login().then((res) {
      if (kDebugMode) print(res.statusCode);
    });

    super.initState();
  }

  @override
  void dispose() {
    _player.dispose();
    super.dispose();
  }

  void _screenshot() async {
    final size = MediaQuery.of(context).size;
    final path = await FilePicker.platform.saveFile(
      dialogTitle: 'Screenshot speichern',
      fileName: '${DateTime.now().microsecondsSinceEpoch}.png',
      type: FileType.image,
    );

    if (path != null) {
      _player.takeSnapshot(File(path), size.width.toInt(), size.height.toInt());
    }
  }

  // void _playPause() {
  //   setState(() => _isPlaying = !_isPlaying);
  //   FindCamDlg.show(context).then((ip) => print(ip));
  //   _player.playOrPause();
  // }

  @override
  Widget build(BuildContext context) {
    final iconColor = kSecondary.withOpacity(0.5);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              gradient: kBackgroundGradient,
            ),
            child: Video(
              fillColor: Colors.transparent,
              fit: BoxFit.cover,
              player: _player,
              height: size.width,
              width: size.height,
              scale: 1.0,
              showControls: false,
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: WindowTitleBarBox(
              child: ClipRRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 15,
                    sigmaY: 15,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: backgroundGradient2tWithOpacity([0.15, 0.3]),
                      border: Border(
                        bottom: BorderSide(
                          width: 2,
                          color: kShadow.withOpacity(0.25),
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        Expanded(child: MoveWindow()),
                        const WindowButtons(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          ControlPanel(
            ptzControl: _client.ptzMove,
            ptzStop: _client.ptzStop,
            leading: [
              ControlPanelButton(
                icon: Icon(
                  Icons.screenshot_monitor_rounded,
                  size: 48,
                  color: iconColor,
                ),
                onPressed: _screenshot,
              ),
            ],
            tail: [
              ControlPanelButton(
                icon: Icon(
                  Icons.zoom_in_rounded,
                  size: 40,
                  color: iconColor,
                ),
                onDown: (_) => _client.zoom(PtzCtrl.zoomIn),
                onUp: (_) => _client.zoomStop(),
              ),
              ControlPanelButton(
                icon: Icon(
                  Icons.zoom_out_rounded,
                  size: 40,
                  color: iconColor,
                ),
                onDown: (_) => _client.zoom(PtzCtrl.zoomOut),
                onUp: (_) => _client.zoomStop(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
