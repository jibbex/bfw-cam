import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';

const Color kBackground = Color(0xffccc6c6);
const Color kLightBackground = Color(0xffd5d0d0);
const Color kPrimaryIcon = Color(0x4a093452);
const Color kPrimary = Color(0x880b3e62);
const Color kAltPrimary = Color(0x91093a4f);
const Color kHover = Color(0xfff1f1f1);
const Color kOnPrimary = Color(0xffbdbbbb);
const Color kSecondary = Color(0xffe0e0e0);
const Color kError = Color(0xffa93245);
const Color kLightError = Color(0xff982c3c);
const Color kGmBtnTextColor = Color(0x33343030);
const Color kSecondaryText = Color(0xaa8f8d8d);
const Color kPrimaryText = Color(0xff2f2b2e);
const Color kBtnBackground = Color(0x8b11252d);
const Color kShadow = Color(0x7111252d);

final buttonColors = WindowButtonColors(
  normal: kBtnBackground,
  mouseOver: kAltPrimary,
  mouseDown: kPrimary,
  iconNormal: kOnPrimary,
  iconMouseOver: kHover,
  iconMouseDown: kSecondary,
);

final closeButtonColors = WindowButtonColors(
  normal: const Color(0xFFB71C1C).withOpacity(0.5),
  mouseOver: const Color(0xFFD32F2F).withOpacity(0.5),
  mouseDown: const Color(0xFFB71C1C).withOpacity(0.25),
  iconNormal: kOnPrimary,
  iconMouseOver: kHover,
);

Gradient kBackgroundGradient = const LinearGradient(
  transform: GradientRotation(0.7),
  stops: [ 0.0, 1.0],
  colors: <Color>[
    kAltPrimary,
    kPrimary,
  ],
);

Gradient kBackgroundGradient2 = const RadialGradient(
  radius: 0.75,
  stops: [0.0, 1.0],
  colors: <Color>[
    kOnPrimary,
    kLightBackground,
  ],
);

Gradient kBackgroundGradientError = LinearGradient(
  transform: const GradientRotation(0.7),
  stops: const [0.0, 1.0],
  colors: <Color>[
    kLightError,
    kError.withOpacity(1),
  ],
);

Gradient backgroundGradientWithOpacity(List<double> opacities) =>
    RadialGradient(
      transform: const GradientRotation(1.570796),
      stops: const [0.0, 1.0],
      colors: <Color>[
        kLightBackground.withOpacity(opacities[0]),
        kLightBackground.withOpacity(opacities[1]),
      ],
    );

Gradient backgroundGradient2tWithOpacity(List<double> opacities) =>
    LinearGradient(
      transform: const GradientRotation(1.570796),
      stops: const [0.0, 1.0],
      colors: <Color>[
        kLightBackground.withOpacity(opacities[0]),
        kOnPrimary.withOpacity(opacities[1]),
      ],
    );
