import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'colors.dart';

const double kGmBtnFontSize = 40;

const Set<MaterialState> interactiveStates = <MaterialState>{
  MaterialState.pressed,
  MaterialState.hovered,
  MaterialState.focused,
};

TextStyle gmBtnTextStyle = TextStyle(
  fontSize: kGmBtnFontSize,
  color: kGmBtnTextColor.withOpacity(0.3),
  fontFamily: 'Condensed',
  fontWeight: FontWeight.w200,
);

EdgeInsets getContainerSpacing(BuildContext context) {
  Size deviceSize = MediaQuery.of(context).size;

  return EdgeInsets.symmetric(
    vertical: 15,
    horizontal: deviceSize.width > 960
        ? deviceSize.width * 0.25
        : deviceSize.width * 0.05,
  );
}

TextStyle eqTextStyle(BuildContext context) =>
    Theme.of(context).textTheme.headline2!.copyWith(
      color: Colors.white.withOpacity(0.4),
      leadingDistribution: TextLeadingDistribution.even,
      fontWeight: FontWeight.w100,
      decorationColor: kPrimary,
      fontSize: 18,
      shadows: [
        Shadow(
          color: Colors.black.withOpacity(0.25),
          offset: Offset.fromDirection(1.1, 2.75),
        )
      ],
    );

Widget getTitle(BuildContext context, String title, [TextStyle? style]) {
  final textStyle = Theme.of(context).textTheme.headline2!.copyWith(
        color: kGmBtnTextColor,
        leadingDistribution: TextLeadingDistribution.even,
        fontWeight: FontWeight.w200,
        decorationColor: kOnPrimary,
        fontSize: style != null ? null : 24,
      );

  return Text(
    title,
    overflow: TextOverflow.fade,
    textAlign: TextAlign.center,
    style: textStyle.merge(style),
  );
}

ButtonStyle buttonStyle = ButtonStyle(
  elevation: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
    if (states.any(interactiveStates.contains)) {
      return 2;
    }

    return 8;
  }),
  padding: MaterialStateProperty.resolveWith(
    (Set<MaterialState> states) => const EdgeInsets.all(8),
  ),
  backgroundColor:
      MaterialStateProperty.resolveWith((Set<MaterialState> states) {
    if (states.any(interactiveStates.contains)) {
      return kAltPrimary.withOpacity(0.25);
    }

    // return Colors.black.withOpacity(0.25);
    return Colors.transparent;
  }),
  foregroundColor:
      MaterialStateProperty.resolveWith((Set<MaterialState> states) {
    if (states.any(interactiveStates.contains)) {
      return kPrimaryIcon.withOpacity(0.5);
    }

    return kPrimaryIcon.withOpacity(0.85);
    // return Colors.transparent;
  }),
  shape: MaterialStateProperty.resolveWith(
    (Set<MaterialState> states) => const CircleBorder(),
  ),
);

TextStyle headline1 = const TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w500,
  color: kPrimaryText,
);

ThemeData _buildTheme(BuildContext context, bool isProviderSet) {
  const int kOverlayAlpha = 0x1f;
  final base = ThemeData.light();

  final titleTextStyle = base.textTheme.titleLarge?.copyWith(
    color: kPrimaryText,
    fontSize: 20,
  );

  final iconTheme = base.iconTheme.copyWith(
    size: 24,
    color: Colors.white.withOpacity(.6),
  );

  return base.copyWith(
      useMaterial3: true,
      primaryColor: kPrimary,
      splashColor: kShadow.withOpacity(0.4),
      hoverColor: kShadow.withOpacity(0.15),
      highlightColor: kPrimary.withOpacity(0.05),
      applyElevationOverlayColor: true,
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: ZoomPageTransitionsBuilder(),
          TargetPlatform.windows: ZoomPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        },
      ),
      colorScheme: base.colorScheme.copyWith(
        primary: kPrimary,
        onPrimary: kOnPrimary,
        secondary: kSecondary,
        error: kError,
        background: kBackground,
      ),
      canvasColor: kBackground,
      backgroundColor: kBackground,
      scaffoldBackgroundColor: kBackground,
      cardTheme: base.cardTheme.copyWith(
        color: kLightBackground,
        surfaceTintColor: Colors.transparent,
        shadowColor: Colors.transparent,
        margin: const EdgeInsets.all(0),
        elevation: 6,
      ),
      textTheme: base.textTheme.copyWith(
        bodyText1: base.textTheme.bodyText1?.copyWith(
          color: kSecondaryText,
          fontWeight: FontWeight.w300,
        ),
        headline1: base.textTheme.headline1?.copyWith(
          fontSize: 18,
          fontWeight: FontWeight.w500,
          color: kPrimaryText,
        ),
        headline2: base.textTheme.headline2?.copyWith(
          color: Colors.white.withOpacity(0.4),
          leadingDistribution: TextLeadingDistribution.even,
          fontWeight: FontWeight.w100,
          decorationColor: kPrimary,
          fontSize: 24,
        ),
      ),
      dataTableTheme: base.dataTableTheme.copyWith(
        decoration: const BoxDecoration(
          color: Colors.transparent,
          backgroundBlendMode: BlendMode.softLight,
        ),
        dataTextStyle: TextStyle(
          color: kSecondaryText,
          fontSize: 16,
          fontWeight: FontWeight.w400,
          fontFamily: 'IBMPlex',
          shadows: [
            Shadow(
              color: Colors.black.withOpacity(0.25),
              offset: Offset.fromDirection(1.1, 2.75),
            )
          ],
        ),
        headingTextStyle: base.textTheme.headline2!.copyWith(
          color: kSecondary.withOpacity(0.8),
          leadingDistribution: TextLeadingDistribution.even,
          fontWeight: FontWeight.w200,
          decorationColor: kPrimary.withOpacity(0.8),
          fontSize: 24,
        ),
      ),
      radioTheme: base.radioTheme.copyWith(
        fillColor:
            MaterialStateProperty.resolveWith((Set<MaterialState> states) {
          if (states.contains(MaterialState.selected)) {
            return kPrimary;
          }

          return kShadow;
        }),
        overlayColor:
            MaterialStateProperty.resolveWith((Set<MaterialState> states) {
          if (states.contains(MaterialState.selected)) {
            return kPrimary.withAlpha(0x1C);
          }

          return kPrimary.withAlpha(kOverlayAlpha);
        }),
      ),
      switchTheme: base.switchTheme.copyWith(thumbColor:
          MaterialStateProperty.resolveWith((Set<MaterialState> states) {
        if (states.contains(MaterialState.selected)) {
          return kPrimary;
        }

        return Colors.grey.shade500.withOpacity(0.5);
      }), trackColor:
          MaterialStateProperty.resolveWith((Set<MaterialState> states) {
        if (states.contains(MaterialState.selected)) {
          return kOnPrimary.withOpacity(0.5);
        }

        return Colors.grey.shade200.withAlpha(kOverlayAlpha);
      }), overlayColor:
          MaterialStateProperty.resolveWith((Set<MaterialState> states) {
        if (states.contains(MaterialState.selected)) {
          return kPrimary.withAlpha(0x1C);
        }

        return kPrimary.withAlpha(kOverlayAlpha);
      })),
      listTileTheme: base.listTileTheme.copyWith(
        horizontalTitleGap: 5,
        minVerticalPadding: 5,
        selectedColor: kPrimary.withOpacity(.75),
        contentPadding: const EdgeInsets.all(2),
        textColor: kPrimaryText,
        iconColor: kPrimary.withOpacity(0.8),
      ),
      popupMenuTheme: base.popupMenuTheme.copyWith(
        color: kLightBackground,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        elevation: 8,
        enableFeedback: true,
      ),
      dialogTheme: base.dialogTheme.copyWith(
        backgroundColor: kLightBackground,
        elevation: 10,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        contentTextStyle: base.dialogTheme.contentTextStyle?.copyWith(
          fontWeight: FontWeight.w400,
        ),
        titleTextStyle: base.dialogTheme.titleTextStyle?.copyWith(
          fontWeight: FontWeight.w400,
        ),
      ),
      timePickerTheme: TimePickerTheme.of(context).copyWith(
        backgroundColor: kBackground,
        hourMinuteShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        dayPeriodShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
      ),
      primaryIconTheme: base.primaryIconTheme.copyWith(
        color: kPrimaryIcon,
      ),
      appBarTheme: base.appBarTheme.copyWith(
        titleTextStyle: titleTextStyle,
        iconTheme: iconTheme,
        actionsIconTheme: iconTheme,
        foregroundColor: kOnPrimary,
        backgroundColor: kBackground.withOpacity(0.25),
        scrolledUnderElevation: 5,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
          systemNavigationBarDividerColor: Colors.white.withOpacity(.5),
          systemNavigationBarColor: kBackground.withOpacity(0.75),
        ),
      ),
      iconTheme: const IconThemeData(
        color: kPrimaryIcon,
      ),
      inputDecorationTheme: base.inputDecorationTheme.copyWith(
        labelStyle: const TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
        fillColor: kSecondary.withOpacity(0.5),
        hoverColor: kShadow.withOpacity(0.1),
        focusColor: kLightBackground,
        filled: true,
        isDense: false,
        contentPadding: const EdgeInsets.all(5),
        helperStyle: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w300,
        ),
        focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          borderSide: BorderSide(
            color: kPrimary,
            width: 3,
          ),
        ),
        enabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          borderSide: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: kBtnBackground.withOpacity(0.5),
        hoverColor: kAltPrimary.withOpacity(0.25),
        splashColor: Colors.black.withOpacity(1),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (interactiveStates.any((e) => states.contains(e))) {
              return kBtnBackground.withOpacity(0.2);
            }

            return kBtnBackground;
          }),
        ),
      ),
      bottomSheetTheme: base.bottomSheetTheme.copyWith(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12.0),
                topRight: Radius.circular(12.0)),
          ),
          modalElevation: 16,
          // modalBackgroundColor: kBackground,
          clipBehavior: Clip.antiAlias,
          backgroundColor: kLightBackground.withOpacity(0.95),
          constraints: const BoxConstraints(
            maxWidth: 460,
          )));
}

ThemeData theme(BuildContext context, [bool isProviderSet = false]) =>
    _buildTheme(context, isProviderSet);
