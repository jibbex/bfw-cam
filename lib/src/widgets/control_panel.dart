import 'dart:ui';

import 'package:bfw_cam/src/hik_client/hik_client.dart';
import 'package:bfw_cam/src/theme/colors.dart';
import 'package:bfw_cam/src/widgets/move_panel.dart';
import 'package:flutter/material.dart';

/// Creates a animated panel a side of the parent widget, that
/// will be slide in, when the mouse cursor enters the area height
/// or width * 1.5. It slides out again after the cursor exits the
/// area.
class ControlPanel extends StatefulWidget {
  final double height;
  final double width;
  final double blur;
  final Alignment alignment;
  final List<ControlPanelButton>? leading;
  final List<Widget>? tail;
  final void Function(Set<Direction>)? ptzControl;
  final void Function()? ptzStop;

  const ControlPanel({
    super.key,
    this.tail,
    this.leading,
    this.ptzStop,
    this.ptzControl,
    this.blur = 15,
    this.height = 110,
    this.width = double.infinity,
    this.alignment = Alignment.bottomCenter,
  });

  @override
  State<ControlPanel> createState() => _ControlPanelState();
}

class _ControlPanelState extends State<ControlPanel>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(milliseconds: 300),
    vsync: this,
  );

  late final Animation<Offset> _offsetAnimation = Tween<Offset>(
    begin: _startOffset,
    end: Offset.zero,
  ).animate(CurvedAnimation(
    parent: _controller,
    curve: Curves.decelerate,
  ));

  late final Animation<double> _fadeAnimation = CurvedAnimation(
    parent: _controller,
    curve: Curves.linear,
  );

  void _onMouseEnter(_) {
    if (!_controller.isAnimating) {
      _controller.forward();
    }
  }

  void _onMouseLeave(_) {
    if (!_controller.isAnimating) {
      _controller.reverse();
    }
  }

  Offset get _startOffset {
    if (widget.alignment == Alignment.bottomCenter) {
      return const Offset(0.0, 1.0);
    } else if (widget.alignment == Alignment.topCenter) {
      return const Offset(0.0, -1.0);
    } else if (widget.alignment == Alignment.centerLeft) {
      return const Offset(-1.0, 0.0);
    }

    return const Offset(1.0, 0.0);
  }

  double? get _widthFactor => widget.alignment == Alignment.topCenter ||
          widget.alignment == Alignment.bottomCenter
      ? 1
      : null;

  double? get _heightFactor => widget.alignment == Alignment.centerLeft ||
          widget.alignment == Alignment.centerRight
      ? 1
      : null;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: widget.alignment,
      widthFactor: _widthFactor,
      heightFactor: _heightFactor,
      child: MouseRegion(
        onEnter: _onMouseEnter,
        onExit: _onMouseLeave,
        child: Container(
          height: widget.height != double.infinity ? widget.height * 2 : null,
          width: widget.width != double.infinity ? widget.width * 2 : null,
          padding: EdgeInsets.only(
            top: widget.height != double.infinity ? widget.height : 0,
          ),
          child: FadeTransition(
            opacity: _fadeAnimation,
            child: SlideTransition(
              position: _offsetAnimation,
              child: ClipRRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: widget.blur,
                    sigmaY: widget.blur,
                  ),
                  child: Container(
                    height:
                        widget.height != double.infinity ? widget.height : null,
                    width:
                        widget.width != double.infinity ? widget.width : null,
                    decoration: BoxDecoration(
                      gradient: backgroundGradient2tWithOpacity([0.3, 0.15]),
                      border: Border(
                        top: BorderSide(
                          width: 2,
                          color: kShadow.withOpacity(0.25),
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        const SizedBox(width: 10),
                        if (widget.leading != null)
                          ...(widget.leading!.map((e) => Row(
                                children: [e, const SizedBox(width: 5)],
                              ))),
                        const Spacer(),
                        MovePanel(
                          size: 100,
                          onMove: widget.ptzControl!,
                          onStop: widget.ptzStop!,
                        ),
                        const SizedBox(width: 5),
                        if (widget.tail != null)
                          ...[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: widget.tail!,

                            ),
                            const SizedBox(width: 5),
                          ],
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ControlPanelButton extends StatelessWidget {
  final double radius;
  final Color color;
  final double size;
  final Widget icon;
  final void Function()? onPressed;
  final void Function(PointerDownEvent)? onDown;
  final void Function(PointerUpEvent)? onUp;

  const ControlPanelButton({
    super.key,
    this.color = kBtnBackground,
    this.size = 48,
    this.radius = 6,
    this.onPressed,
    this.onDown,
    this.onUp,
    required this.icon,
  });

  void _onPressed() {
    if (onPressed != null) {
      onPressed!();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: onDown,
      onPointerUp: onUp,
      child: ElevatedButton(
        onPressed: _onPressed,
        // style: buttonStyle,
        child: Padding(
          padding: const EdgeInsets.all(2),
          child: icon,
        ),
      ),
    );
  }
}
