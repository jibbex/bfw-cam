import 'dart:async';
import 'dart:io';
import 'package:bfw_cam/src/hik_client/hik_client.dart';
import 'package:bfw_cam/src/ipv4_formatter.dart';
import 'package:bfw_cam/src/theme/colors.dart';
import 'package:bfw_cam/src/theme/theme.dart';
import 'package:bfw_cam/src/widgets/ipv4_text_edit.dart';
import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FindCamDlg extends StatefulWidget {
  final Completer<InternetAddress> _ipAddress = Completer<InternetAddress>();

  FindCamDlg({super.key});

  static Future<InternetAddress> show(BuildContext context) {
    final dlg = FindCamDlg();
    showDialog(context: context, builder: (context) => dlg);

    return dlg._ipAddress.future;
  }

  @override
  State<FindCamDlg> createState() => _FindCamDlgState();
}

class _FindCamDlgState extends State<FindCamDlg> {
  int _current = 0;
  int _total = 0;
  bool _scanning = false;
  final _netController = TextEditingController();
  final _maskController = TextEditingController();
  NetworkInterface? _selectedInterface;

  String? getNetwork() => _selectedInterface?.addresses.first.address;

  @override
  void initState() {
    _netController.text = '0.0.0.0';
    super.initState();
  }

  @override
  void dispose() {
    _netController.dispose();
    _maskController.dispose();
    super.dispose();
  }

  Widget _ifaceFutureBuilder(
    BuildContext context,
    AsyncSnapshot<List<NetworkInterface>> snapshot,
  ) {
    if (snapshot.hasData) {
      _selectedInterface ??= snapshot.data![0];

      return Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: kSecondary.withOpacity(0.5),
                  ),
                  child: Container(),
                ),
                DropdownButton<String>(
                  isExpanded: false,
                  borderRadius: BorderRadius.circular(6),
                  dropdownColor: kSecondary,
                  underline: Container(),
                  value: _selectedInterface!.name,
                  items: snapshot.data!
                      .map<DropdownMenuItem<String>>(
                        (e) => DropdownMenuItem<String>(
                          value: e.name,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Text(e.name),
                          ),
                        ),
                      )
                      .toList(),
                  onChanged: (String? iface) {
                    setState(() {
                      _selectedInterface =
                          snapshot.data!.firstWhere((e) => e.name == iface);
                      _netController.value = IPv4Formatter().formatEditUpdate(
                        TextEditingValue(
                          text: getNetwork()!,
                        ),
                        TextEditingValue(
                          text: getNetwork()!,
                        ),
                      );
                    });
                  },
                ),
              ],
            ),
            const SizedBox(height: 25),
            IPv4TextEdit(
              netController: _netController,
              maskController: _maskController,
            )
          ],
        ),
      );
    }

    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  double? _calcProgress() {
    if (_total == 0 || _current == 0) {
      return null;
    }

    return _current / _total;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final titleStyle = theme.textTheme.headline1!.copyWith(
      fontWeight: FontWeight.w200,
    );

    return AlertDialog(
      icon: const Icon(
        Icons.lan_rounded,
        size: 32,
        color: kPrimary,
      ),
      iconPadding: const EdgeInsets.all(10),
      title: Text(
        'IP Kamera finden',
        style: titleStyle,
      ),
      content: !_scanning
          ? FutureBuilder(
              future: NetworkInterface.list(),
              builder: _ifaceFutureBuilder,
            )
          : LinearProgressIndicator(
              minHeight: 8,
              value: _calcProgress(),
              semanticsLabel: 'Suche läuft',
            ),
      actions: [
        TextButton(
          onPressed: _scanning
              ? null
              : () async {
                  setState(() => _scanning = true);
                  (await HikClient.discover(
                    _netController.text,
                    _maskController.text,
                  ))
                      .listen((data) {
                    if (data is int) {
                      setState(() => _total = data);
                    } else {
                      if (data is String || data is SocketException) {
                        if (kDebugMode) {
                          print(data);
                        }
                      } else if (data is HttpHeaders) {
                        final address = InternetAddress(data.host!,
                            type: InternetAddressType.IPv4);

                        widget._ipAddress.complete(address);
                      }

                      setState(() {
                        _current++;
                        if (_current == _total) {
                          _scanning = false;
                          Navigator.of(context).pop();
                          AnimatedSnackBar(
                            builder: ((context) {
                              return Container(
                                padding: const EdgeInsets.all(5),
                                width: 600,
                                decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                    gradient: kBackgroundGradientError,
                                    color: kLightError,
                                    boxShadow: const [
                                      BoxShadow(
                                        color: kShadow,
                                        blurRadius: 5,
                                        blurStyle: BlurStyle.outer,
                                      ),
                                    ]),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.error_rounded,
                                      size: 48,
                                      color: kOnPrimary.withOpacity(0.75),
                                    ),
                                    Expanded(
                                      child: getTitle(
                                        context,
                                        'Es wurde keine Kamera gefunden',
                                        const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }),
                            desktopSnackBarPosition:
                                DesktopSnackBarPosition.topCenter,
                            duration: const Duration(seconds: 25),
                          ).show(context);
                        }
                      });
                    }
                  });
                },
          child: const Text('suchen'),
        ),
      ],
    );
  }
}
