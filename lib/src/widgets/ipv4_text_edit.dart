import 'package:bfw_cam/src/ipv4_formatter.dart';
import 'package:bfw_cam/src/number_formatter.dart';
import 'package:bfw_cam/src/theme/colors.dart';
import 'package:flutter/material.dart';

class IPv4TextEdit extends StatefulWidget {
  final TextEditingController netController;
  final TextEditingController maskController;
  final TextEditingValue? value;

  const IPv4TextEdit({
    super.key,
    this.value,
    required this.netController,
    required this.maskController,
  });

  @override
  State<IPv4TextEdit> createState() => _IPv4TextEditState();
}

class _IPv4TextEditState extends State<IPv4TextEdit> {
  late final TextEditingController _netController;
  late final TextEditingController _maskController;

  @override
  void initState() {
    _maskController = widget.maskController;
    _netController = widget.netController;

    _maskController.text = '0';
    _netController.value = widget.value ??
        IPv4Formatter().formatEditUpdate(
          const TextEditingValue(
            text: '0.0.0.0',
          ),
          const TextEditingValue(
            text: '0.0.0.0',
          ),
        );

    _netController.addListener(() => _onNetChanged(_netController.text));

    super.initState();
  }

  @override
  void dispose() {
    // This should be covered by disposal in the parent.
    // TODO: However, keep an eye out for unexpected behaviour.
    //
    // _netController.dispose();
    // _maskController.dispose();
    super.dispose();
  }

  void _onNetChanged(String value) {
    if (RegExp(r'^(\d{1,3}\.){3}\d{1,3}$').hasMatch(value)) {
      int mask = 0;
      final firstByte = int.tryParse(value.split('.')[0]);

      if (firstByte != null) {
        if (firstByte <= 127) {
          mask = 8;
        } else if (firstByte > 127 && firstByte <= 191) {
          mask = 16;
        } else if (firstByte > 191 && firstByte <= 254) {
          mask = 24;
        }

        _maskController.text = mask.toString();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: kSecondary.withOpacity(0.5),
                ),
                child: Container(),
              ),
              TextField(
                textAlign: TextAlign.center,
                cursorWidth: 1,
                controller: _netController,
                inputFormatters: [
                  IPv4Formatter(),
                ],
              ),
            ],
          ),
        ),
        const Text(' / '),
        Expanded(
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: kSecondary.withOpacity(0.5),
                ),
                child: Container(),
              ),
              TextField(
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                cursorWidth: 1,
                controller: _maskController,
                inputFormatters: [
                  NumberFormatter(
                    maxDigits: 2,
                    maxValue: 31,
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
