import 'dart:math';

import 'package:bfw_cam/src/hik_client/hik_client.dart' show Direction;
import 'package:bfw_cam/src/theme/colors.dart';
import 'package:flutter/material.dart';

class MovePanel extends StatefulWidget {
  final double size;
  final void Function(Set<Direction>) onMove;
  final void Function() onStop;

  const MovePanel({
    super.key,
    this.size = 100,
    required this.onMove,
    required this.onStop,
  });

  @override
  State<MovePanel> createState() => _MovePanelState();
}

class _MovePanelState extends State<MovePanel> {
  final _controller = MaterialStatesController();
  Set<Direction> _direction = {};
  Alignment align = const Alignment(0, 0);

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onCancel(PointerEvent event) {
    if (align.x != 0 || align.y != 0) {
      widget.onStop();
      setState(() {
        align = const Alignment(0, 0);
        _direction = {};
      });
    }
  }

  Alignment _pointerAlignment(PointerEvent event) {
    final x = pi * event.localPosition.dx;
    final y = pi * event.localPosition.dy;
    final breakpoints = [
      pi / 3 * widget.size,
      pi / 2 * widget.size,
    ];

    double dx = 0;
    double dy = 0;

    if (x < breakpoints[0]) {
      dx = -0.75;
    } else if (x > breakpoints[0] && x < breakpoints[1]) {
      dx = 0;
    } else {
      dx = 0.75;
    }

    if (y < breakpoints[0]) {
      dy = -0.75;
    } else if (y > breakpoints[0] && y < breakpoints[1]) {
      dy = 0;
    } else {
      dy = 0.75;
    }

    return Alignment(dx, dy);
  }

  void _onMove(PointerEvent event) {
    Set<Direction> direction = {};
    Alignment alignment = _pointerAlignment(event);

    if (alignment.x < 0) {
      direction.add(Direction.left);
    } else if (alignment.x > 0) {
      direction.add(Direction.right);
    }

    if (alignment.y < 0) {
      direction.add(Direction.top);
    } else if (alignment.y > 0) {
      direction.add(Direction.bottom);
    }

    setState(() {
      if (align.x != alignment.x || align.y != alignment.y) {
        align = alignment;
        _direction = direction;
        _controller.update(MaterialState.pressed, true);
        if (direction.isEmpty) {
          widget.onStop();
        } else {
          widget.onMove(_direction);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerUp: _onCancel,
      onPointerCancel: _onCancel,
      onPointerDown: _onMove,
      onPointerMove: _onMove,
      child: ElevatedButton(
        onPressed: () {
        },
        statesController: _controller,
        style: Theme.of(context).elevatedButtonTheme.style!.copyWith(
              padding: const MaterialStatePropertyAll(
                EdgeInsets.all(0),
              ),
              shape: MaterialStatePropertyAll(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
              fixedSize: MaterialStatePropertyAll(
                Size(
                  widget.size,
                  widget.size,
                ),
              ),
            ),
        child: MouseRegion(
          child: Stack(
            alignment: Alignment.center,
            children: [
              AnimatedAlign(
                alignment: align,
                duration: const Duration(milliseconds: 100),
                child: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    color: kSecondary.withOpacity(0.5),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
